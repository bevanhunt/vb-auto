Option Explicit

Dim automobile 

Set automobile = new Auto
automobile.Model = "3" 'SUV'
automobile.InteriorColor = "5" 'Pink'
assert_equal "Pink", automobile.InteriorColor, "SUV:Pink:" _
& "Interior Color Should be Pink"

Set automobile = new Auto
automobile.Model = "2" 'Convertible'
automobile.InteriorColor = "5" 'Pink'
assert_not_equal "Pink", automobile.InteriorColor, "Convertible:Pink:" _
& "Interior Color Should Not be Pink"

Set automobile = new Auto
automobile.Model = "2" 'Convertible'
automobile.Transmission = "1" 'Techtronic'
assert_equal "Techtronic", automobile.Transmission, "Convertible:Techronic:" _
& "Transmission Should be Techtronic"

Set automobile = new Auto
automobile.Model = "1" 'Mid-Size'
automobile.Transmission = "1" 'Techtronic'
assert_equal "Techtronic", automobile.Transmission, "Mid-Size:Techronic:" _
& "Transmission Should be Techtronic"

Set automobile = new Auto
automobile.Model = "0" 'Small Sedan'
automobile.Transmission = "1" 'Techtronic'
assert_not_equal "Techtronic", automobile.Transmission, "Small Sedan:Techtronic" _
& "Transmission Should Not be Techtronic"

Set automobile = new Auto
automobile.Model = "3" 'SUV'
automobile.Transmission = "1" 'Techtronic'
assert_not_equal "Techtronic", automobile.Transmission, "SUV:Techronic: " _
& "Transmission Should Not be Techtronic"

Set automobile = new Auto
automobile.Model = "3" 'SUV'
automobile.Transmission = "1" 'Techtronic'
automobile.AV = "1" 'Full Surrond Sound'
automobile.ExteriorColor = "1" 'White'
automobile.InteriorColor = "1" 'Grey'
assert_not_equal "", automobile.InteriorColor, "Interior Color should exist"
assert_not_equal "", automobile.ExteriorColor, "ExteriorColor should exist"
assert_not_equal "", automobile.AV, "AV should exist"

Set automobile = new Auto
automobile.Model = "55" 'OOB'
assert_equal "", automobile.Model, "Bounds Check Fail"
automobile.Model = "-1" 'OOB'
assert_equal "", automobile.Model, "Bounds Check Fail"
automobile.Transmission = "55" 'OOB'
assert_equal "", automobile.Transmission, "Bounds Check Fail"
automobile.Transmission = "-1" 'OOB'
assert_equal "", automobile.Transmission, "Bounds Check Fail"
automobile.AV = "55" 'OOB'
assert_equal "", automobile.AV, "Bounds Check Fail"
automobile.AV = "-1" 'OOB'
automobile.ExteriorColor = "55" 'OOB'
assert_equal "", automobile.ExteriorColor, "Bounds Check Fail"
automobile.ExteriorColor = "-1" 'OOB'
assert_equal "", automobile.ExteriorColor, "Bounds Check Fail"
automobile.InteriorColor = "55" 'OOB'
assert_equal "", automobile.InteriorColor, "Bounds Check Fail"
automobile.InteriorColor = "-1" 'OOB'
assert_equal "", automobile.InteriorColor, "Bounds Check Fail"

Class Auto
	Private strInteriorColor
	Private strExteriorColor
	Private strModel
	Private strTransmission
	Private strAV
	Private astrInteriorColors
	Private astrExteriorColors
	Private astrModels
	Private astrTransmissions
	Private astrAVs
	'Set the intial values for the Auto object'
	Private Sub Class_Initialize
		astrModels = Array("Small Sedan","Mid-Size","Convertible","SUV") 
		astrInteriorColors = Array( _
			"Black", _
			"Grey", _
			"White", _
			"Yellow", _
			"Red", _
			"Pink")
		astrExteriorColors = Array( _
			"Black", _
			"White", _
			"Red", _
			"Blue", _
			"Green")
		astrAVs = Array( _
			"Basic 4 Speakers", _
			"Full Surround Sound", _
			"Full Surrond Sound with 360 Cameras")
		astrTransmissions = Array( _ 
			"Manual", _
			"Techtronic", _
			"6 Speed Automatic")
	End Sub
	Public Property Get Models()
		Models = astrModels
	End Property  
	Public Property Let Model(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrModels) And _
		Clng(strNum) >= LBound(astrModels) Then
			strModel = astrModels(strNum)
		Else 
		End If 
	End Property 
	Public Property Get Model
		Model = strModel
	End Property
	Public Property Get InteriorColor
		InteriorColor = strInteriorColor
	End Property
	Public Property Get InteriorColors()
		InteriorColors = astrInteriorColors
	End Property
	Public Property Let InteriorColor(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrInteriorColors) And _
		Clng(strNum) >= LBound(astrInteriorColors) Then
			'only the SUV can have a pink interior'
			If Clng(strNum) = 5 Then
				If strModel = "SUV" Then
					strInteriorColor = astrInteriorColors(strNum)
				Else
				End If
			Else
				strInteriorColor = astrInteriorColors(strNum)
			End If
		Else 
		End If 
	End Property
	Public Property Get ExteriorColor
		ExteriorColor = strExteriorColor
	End Property
	Public Property Get ExteriorColors()
		ExteriorColors = astrExteriorColors
	End Property
	Public Property Let ExteriorColor(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrExteriorColors) And _
		Clng(strNum) >= LBound(astrExteriorColors) Then
			strExteriorColor = astrExteriorColors(strNum)
		Else 
		End If 
	End Property
	Public Property Get Transmission
		Transmission = strTransmission
	End Property
	Public Property Get Transmissions()
		Transmissions = astrTransmissions
	End Property
	Public Property Let Transmission(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrTransmissions) And _
		Clng(strNum) >= LBound(astrTransmissions) Then
			'only the convertible and mid-size can have a tectronic transmission'
			If Clng(strNum) = 1 Then
				If strModel = "Convertible" Or strModel = "Mid-Size" Then
					strTransmission = astrTransmissions(strNum)
				Else
				End If
			Else
				strTransmission = astrTransmissions(strNum)
			End If
		Else 
		End If 
	End Property
	Public Property Get AV
		AV = strAV
	End Property
	Public Property Get AVs()
		AVs = astrAVs
	End Property
	Public Property Let AV(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrAVs) And _
		Clng(strNum) >= LBound(astrAVs) Then
			strAV = astrAVs(strNum)
		Else 
		End If 
	End Property
End Class