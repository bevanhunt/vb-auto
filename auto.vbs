Option Explicit

'main program'
Dim automobile 
Set automobile = new Auto
Welcome
automobile.Model = Modal("Please choose a model.", automobile.Models)
automobile.InteriorColor = Modal("Please choose an interior color.", automobile.InteriorColors)
automobile.ExteriorColor = Modal("Please choose an exterior color.", automobile.ExteriorColors)
automobile.Transmission = Modal("Please choose a transmission.", automobile.Transmissions)
automobile.AV = Modal("Please choose an AV system.", automobile.AVs)
Dim strMsg
strMsg = "Your Auto: " & Chr(13)
strMsg = strMsg & "Model: " & automobile.Model & Chr(13)
strMsg = strMsg & "Interior Color: " & automobile.InteriorColor & Chr(13)
strMsg = strMsg & "Exterior Color: " & automobile.ExteriorColor & Chr(13)
strMsg = strMsg & "Transmission: " & automobile.Transmission & Chr(13)
strMsg = strMsg & "AV: " & automobile.AV & Chr(13)
MsgBox(strMsg)

'welcome message'
Sub Welcome
	MsgBox "Welcome to Bevan Auto Co."
End Sub

'modal generator'
Function Modal(strMsg, collection)
	Dim i, strInput
	strMsg = strMsg & Chr(13)
	For i=0 To UBound(collection) Step 1
		strMsg = strMsg &_ 
		Chr(13) &_ 
		Cstr(i) &_ 
		". " &_ 
		collection(i) &_ 
		Chr(13)
	Next
	strInput = InputBox(strMsg, "Bevan Auto Co.") 
	Modal = strInput
End Function

Class Auto
	Private strInteriorColor
	Private strExteriorColor
	Private strModel
	Private strTransmission
	Private strAV
	Private astrInteriorColors
	Private astrExteriorColors
	Private astrModels
	Private astrTransmissions
	Private astrAVs
	'Set the intial values for the Auto object'
	Private Sub Class_Initialize
		astrModels = Array("Small Sedan","Mid-Size","Convertible","SUV") 
		astrInteriorColors = Array( _
			"Black", _
			"Grey", _
			"White", _
			"Yellow", _
			"Red", _
			"Pink")
		astrExteriorColors = Array( _
			"Black", _
			"White", _
			"Red", _
			"Blue", _
			"Green")
		astrAVs = Array( _
			"Basic 4 Speakers", _
			"Full Surround Sound", _
			"Full Surrond Sound with 360 Cameras")
		astrTransmissions = Array( _ 
			"Manual", _
			"Tectronic", _
			"6 Speed Automatic")
	End Sub
	Public Property Get Models()
		Models = astrModels
	End Property  
	Public Property Let Model(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrModels) And _
		Clng(strNum) >= LBound(astrModels) Then
			strModel = astrModels(strNum)
		Else 
			MsgBox("Please Choose a number that is listed.")
			strModel = Modal("Please choose a model.", automobile.Models)
		End If 
	End Property 
	Public Property Get Model
		Model = strModel
	End Property
	Public Property Get InteriorColor
		InteriorColor = strInteriorColor
	End Property
	Public Property Get InteriorColors()
		InteriorColors = astrInteriorColors
	End Property
	Public Property Let InteriorColor(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrInteriorColors) And _
		Clng(strNum) >= LBound(astrInteriorColors) Then
			'only the SUV can have a pink interior'
			If Clng(strNum) = 5 Then
				If strModel = "SUV" Then
					strInteriorColor = astrInteriorColors(strNum)
				Else
					MsgBox("Pink is not available - please choose another color.")
					strInteriorColor = Modal("Please choose an interior color.", automobile.InteriorColors)
				End If
			Else
				strInteriorColor = astrInteriorColors(strNum)
			End If
		Else 
			MsgBox("Please choose a number that is listed.")
			strInteriorColor = Modal("Please choose an interior color.", automobile.InteriorColors)
		End If 
	End Property
	Public Property Get ExteriorColor
		ExteriorColor = strExteriorColor
	End Property
	Public Property Get ExteriorColors()
		ExteriorColors = astrExteriorColors
	End Property
	Public Property Let ExteriorColor(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrExteriorColors) And _
		Clng(strNum) >= LBound(astrExteriorColors) Then
			strExteriorColor = astrExteriorColors(strNum)
		Else 
			MsgBox("Please choose a number that is listed.")
			strExteriorColor = Modal("Please choose an exterior color.", automobile.ExteriorColors)
		End If 
	End Property
	Public Property Get Transmission
		Transmission = strTransmission
	End Property
	Public Property Get Transmissions()
		Transmissions = astrTransmissions
	End Property
	Public Property Let Transmission(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrTransmissions) And _
		Clng(strNum) >= LBound(astrTransmissions) Then
			'only the convertible and mid-size can have a tectronic transmission'
			If Clng(strNum) = 1 Then
				If strModel = "Convertible" Or strModel = "Mid-Size" Then
					strTransmission = astrTransmissions(strNum)
				Else
					MsgBox("Please choose a different transmission.")
					strTransmission = Modal("Please choose a transmission.", automobile.Transmissions)
				End If
			Else
				strTransmission = astrTransmissions(strNum)
			End If
		Else 
			MsgBox("Please choose a number that is listed.")
			strTransmission = Modal("Please choose a transmission.", automobile.Transmissions)
		End If 
	End Property
	Public Property Get AV
		AV = strAV
	End Property
	Public Property Get AVs()
		AVs = astrAVs
	End Property
	Public Property Let AV(strNum)
		'Check Bounds - if error repeat - convert string input into long'
		If Clng(strNum) <= UBound(astrAVs) And _
		Clng(strNum) >= LBound(astrAVs) Then
			strAV = astrAVs(strNum)
		Else 
			MsgBox("Please choose a number that is listed.")
			strAV = Modal("Please choose an AV system.", automobile.AVs)
		End If 
	End Property
End Class